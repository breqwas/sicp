
;; 1.12

(define (pascal row col)
  (cond
   ((or
     (< row 0)
     (> col row))
    0)                                  ; bad inputs
   ((< row 2) 1)                        ; first two rows
   ((= col 0) 1)                        ; left edge
   ((> col (/ row 2)) (pascal row (- row col))) ; mirror right side to left
   (else (+
          (pascal (- row 1) col)
          (pascal (- row 1) (- col 1))))))

(pascal 0 0)
(pascal 2 1)
(pascal 4 0)
(pascal 5 3)
(pascal 6 3)

