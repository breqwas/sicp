
;; 1.37

(define (cont-frac n-i d-i k)
  (define (cont-frac-rec k-cur)
    (/ (n-i k-cur)
       (+ (d-i k-cur)
          (if (= k k-cur)
              0
              (cont-frac-rec (+ k-cur 1))))))
  ;; (cont-frac-rec 1)
  (define (cont-frac-iter k-cur prev-val)
    (if (= k-cur 0)
        prev-val
        (cont-frac-iter (- k-cur 1)
                         (/ (n-i k-cur)
                            (+ (d-i k-cur) prev-val)))))
  (cont-frac-iter k 0))

(cont-frac (lambda (i) 1.0)
           (lambda (i) 1.0)
           100)

;; 1.38

(define (e)
  (define (d-i n)
    (let ((pos (- n 2)))
      (if (= (modulo pos 3) 0)
          (* 2 (+ (/ pos 3) 1))
          1)))
  (+ 2
     (cont-frac (lambda (n) 1.0)
                d-i
                100)))

;; 1.39

(define (tan-cf x k)
  (define (n-i k-cur)
    (if (= k-cur 1)
        x
        (* -1.0
           (* x x))))
  (define (d-i k-cur)
    (- (* k-cur 2)
       1))
  (cont-frac n-i d-i k))




