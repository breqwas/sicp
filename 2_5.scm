;; 2.5

(define (cons a b)
  (* (expt 2 a)
     (expt 3 b)))

(define (find-pow number base)
  (define (find-pow-iter cur-pow rem)
    (if (= (modulo rem base) 0)
        (find-pow-iter (+ cur-pow 1)
                  (/ rem base))
        cur-pow))
  (find-pow-iter 0 number))

(define (car pair)
  (find-pow pair 2))

(define (cdr pair)
  (find-pow pair 3))

(let ((pair (cons 125 87)))
  (newline)
  (display (car pair))
  (display ":")
  (display (cdr pair)))

