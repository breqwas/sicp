
;; 1.46

(define (iterative-improve good-enough? improve)
  (define (ret x)
    (if (good-enough? x)
        x
        (ret (improve x))))
  ret)

(define (sqrt x)
  (define (good-enough? guess)
    (< (abs (- (square guess) x)) 0.001))
  (define (improve guess)
    (average guess (/ x guess)))
  (define (average x y)
    (/ (+ x y) 2.0))

  ((iterative-improve good-enough? improve) 1))

(sqrt 25)

