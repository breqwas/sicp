(define nil (list ))

;; 2.21

(define (square-list items)
  (if (null? items)
      nil
      (cons (square (car items))
            (square-list (cdr items)))))

(define (square-list-map items)
  (map square items))

;; 2.22

(define (square-list-iter items)
 (define (iter things answer)
    (if (null? things)
        answer
        (iter (cdr things)
              (append answer
                      (list (square (car things)))))))
  (iter items nil))
  
;; 2.23

(define (for-each sub items)
  (if (null? items)
      nil
      (let ((res (sub (car items))))
        (for-each sub (cdr items)))))

(for-each (lambda (x)
            (newline)
            (display x))
          (list 1 2 3 4))
