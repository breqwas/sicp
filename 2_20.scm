;; 2.17

(define (last-pair l)
  (list-ref l (- (length l) 1)))

;; 2.18

(define (reverse l)
  (if (= (length l) 1)
      (list (car l))
      (append (reverse (cdr l)) (list (car l)))))

;; 2.20

(define (same-parity x . nums)
  (define (same-parity-rec parity nums)
    (if (null? nums)
        nums
        (if (= parity
               (modulo (car nums) 2))
            (append (list (car nums)) (same-parity-rec parity (cdr nums)))
            (same-parity-rec parity (cdr nums)))))
  (same-parity-rec (modulo x 2) nums))


