;; 1.21-1.24

(define (square n)
  (* n n))


(define (smallest-divisor n)
  (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (next-divisor test-divisor)))))

(define (next-divisor n)
  (if (< n 3)
      (+ n 1)
      (+ n 2)))

(define (divides? a b)
  (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))


(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else false)))



(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (define is-prime (fast-prime? n 5000))
  (if is-prime
      (report-prime (- (runtime) start-time)))
  is-prime)

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))


(define (search-for-primes cur max number)
  (define cur-is-prime (timed-prime-test cur))
  (define new-number (if cur-is-prime (- number 1) number))
  (define new-cur (+ cur 1))
  (cond ((= new-number 0) (display " ...enough"))
        ((= new-cur max) (display " ...done"))
        (else (search-for-primes new-cur max new-number))))

(search-for-primes 1000000000 1500000000 3)

;; regular
;; 100000000057 *** .54
;; 10000000061 *** .24 
;; 1000000021 *** .06

;; next-divisor
;; 100000000057 *** .34

;; fast-prime? x500
;; 100000000057 *** .52
;; 10000000061 *** .459
;; 1000000021 *** .48


