(define nil (list ))


;; 2.27

(define (reverse l)  ; this is better 2.18
  (if (null? l)
      nil
      (append (reverse (cdr l))
              (list (car l)))))

(define (deep-reverse l)
  (if (pair? l)
      (map deep-reverse (reverse l))
      l))
