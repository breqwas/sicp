
;; 1.8

(define (square a)
  (* a a))

(define (cubrt-iter guess prev-guess x)
  (if (good-enough? guess prev-guess)
      guess
      (cubrt-iter (improve guess x)
                  guess
                  x)))

(define (good-enough? guess prev-guess)          ; 1.7
  (define diff (/ (abs (- guess prev-guess) ) guess))
  (< diff 0.001))

(define (improve guess x)
  (/
   (+
    (/ x (square guess))
    (* 2 guess))
   3))

(define (cubrt x)
  (cubrt-iter x (* 2 x) x))

(cubrt 8)

