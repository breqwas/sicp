
;; 1.33

(define (filtered-accumulate filter combiner term a next b null-value)
  (if (> a b)
      null-value
      (if (filter a)
          (combiner (term a)
                    (filtered-accumulate filter combiner term (next a) next b))
          (filtered-accumulate filter combiner term (next a) next b))))


(define (filtered-accumulate-iter filter combiner term a next b null-value)
  (define (iter a result)
    (if (> a b)
        result
        (if (filter a)
            (iter (next a) (combiner result (term a)))
            (iter (next a) result))))
  (iter a null-value))


(define (next a) (+ a 1))
(define (self a) a)
(define (is-even? a) (= (modulo a 2) 0))
(define (sum-even a b)
  (filtered-accumulate-iter is-even? + self a next b 0))

(sum-even 0 100)


















