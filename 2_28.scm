(define nil (list ))


;; 2.28

(define (fringe l)
  (if (pair? l)
      (append (fringe (car l))
              (fringe (cdr l)))
      (if (null? l) l (list l))))

(define x (list (list 1 2) (list (list 3 4) 5 6 (list 7 8))))
(fringe x)
