
;; 1.16

(define (square a)
  (* a a))

(define (half a)
  (/ a 2))


(define (fast-expt b n)
  (cond ((= n 0) 1)
        ((even? n) (square (fast-expt b (/ n 2))))
        (else (* b (fast-expt b (- n 1))))))

(define (even? n)
  (= (remainder n 2) 0))

(fast-expt 2 10)


(define (iter-expt b n)
  (iter-run-expt b n 1))

(define (iter-run-expt base power mult)
  (cond ((= power 1) (* base mult))
        ((even? power) (iter-run-expt (square base) (half power) mult))
        (else (iter-run-expt base (- power 1) (* mult base)))))

(iter-expt 2 10)
