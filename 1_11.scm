
;; 1.11

(define (f n)
  (if (< n 3)
      n
      (+
       (f (- n 1))
       (* 2 (f (- n 2)))
       (* 3 (f (- n 3))))))

(f 5)

(define (f-iter n)
  (f-run-iter n 0 0 0 0))

(define (f-run-iter lim cur-n val-n val-n-1 val-n-2)
  (if (> cur-n lim)
      val-n
      (f-run-iter
       lim
       (+ cur-n 1)
       (if (< cur-n 3)
           cur-n
           (+
            val-n
            (* 2 val-n-1)
            (* 3 val-n-2)))
       val-n
       val-n-1)))

(f-iter 5)
