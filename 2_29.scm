;; 2.29
;; why did I make it that hard?


;; generic high-order stuff

(define (reduce reducer l)
  (if (null? (cdr l))
      (car l)
      (reducer (car l)
               (reduce reducer (cdr l)))))

(define (deep-map mapper                ; mapper for a plain node
                  subnodes              ; func that return subnodes of a node if there's any
                  construct             ; mapper for a non-plain node, takes current node & a list of mapped subnodes
                  data)                 ; tree structure
  
  (if (null? (subnodes data))
      (mapper data)
      (construct data
                 (map (lambda (x)
                        (deep-map mapper subnodes construct x))
                      (subnodes data)))))

(define (deep-reduce reducer subnodes data)  
  (define (self x) x)
  (define (construct cur below)
    (reduce reducer below))
  (deep-map self subnodes construct data))

(define (get-subnodes dive? get)
  (lambda (x)
    (if (dive? x)
        (get x)
        (list))))


;; show that generic stuff works

(define (deep-sum l)
  (define (self x) x)
  (deep-reduce + (get-subnodes pair? self) l))

(define (deep-square l)                 ; whoa, that's 2.30 and 2.31 too!
  (define (self x) x)
  (deep-map square (get-subnodes pair? self) (lambda (cur sub) sub) l))

(deep-sum (deep-square (list (list 1 2) (list (list 3 4) 5 6 (list 7 8)))))
(reduce + (map square (list 1 2 3 4 5 6 7 8)))


;; mobile definitions

(define (make-mobile left right)
  (list left right))

(define (make-branch length structure)
  (list length structure))

(define (left-branch m)
  (car m))
(define (right-branch m)
  (car (cdr m)))

(define (branch-length b)
  (car b))
(define (branch-structure b)
  (car (cdr b)))


;; smart stuff

(define mobile-subnodes
  (get-subnodes pair?
                (lambda (m)
                  (list (branch-structure (left-branch m))
                        (branch-structure (right-branch m))))))

(define (total-weight mobile)           ; 2.19 b
  (if (null? (mobile-subnodes mobile))
      mobile
      (deep-reduce + mobile-subnodes mobile)))

(define (balanced-mobile-local? m)
  (define (torque b)
    (* (branch-length b)
       (total-weight (branch-structure b))))
  (= (torque (left-branch m))
     (torque (right-branch m))))

(define (balanced-mobile? m)            ; 2.19 c
  (deep-map (lambda (m)
              (if (pair? m)
                  (balanced-mobile-local? m)
                  #t))
            mobile-subnodes
            (lambda (cur sub)
              (define (all-true a b)
                (if (and a b) #t #f))
              (and (balanced-mobile-local? cur)
                   (reduce all-true sub)))
            m))


;; check if that works

(define mobile (let ((mm make-mobile)
                     (mb make-branch))
                 (mm (mb 4 6)
                     (mb 2 (mm (mb 1 10)
                               (mb 5 (mm (mb 1 1)
                                         (mb 1 1))))))))

(branch-structure (left-branch mobile))
(branch-length (right-branch mobile))
(total-weight mobile)
(total-weight (branch-structure (left-branch mobile)))
(total-weight (branch-structure (right-branch mobile)))
(balanced-mobile-local? mobile)
(balanced-mobile? mobile)
