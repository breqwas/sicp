
;; 1.4
(define (max a b)
  (if (> a b) a b))

(define (min a b)
  (if (< a b) a b))

(define (sq a)
  (* a a))

(define (maxsq a b c)
  (define first (max a b))
  (define second (max c (min a b)))
  (+ (sq first) (sq second)))

(maxsq 2 3 4)
