
;; 1.30

(define (sum-iter term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (+ result (term a)))))
  (iter a 0))

;; 1.29

(define (integral-simpson f a b n)
  (define h (/ (- b a) n))
  (define (y-k k)
    (f (+ a (* k h))))
  (define (sum-coeff k)
    (cond ((= k 0) 1)
          ((= k n) 1)
          ((= (modulo k 2) 0) 2)
          (else 4)))
  (define (sum-element k)
    (* (sum-coeff k) (y-k k)))
  (define (next a)
    (+ a 1))
  (define sum-res (sum-iter sum-element 0 next n))
  (* (/ h 3.0) sum-res))

(integral-simpson cube 0.0 1.0 10000)
